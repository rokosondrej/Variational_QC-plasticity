function [r2,f_r,K_r,K_rDBC] = grad_hess(atoms,bonds,x,z1,k1,DBCIndices,...
    tDBCValues,FreeIndices,R0,TOL_z,maxNumThreads)

% Reconstruct r2
r2 = zeros(2*length(atoms),1);
r2(DBCIndices) = R0(DBCIndices)+tDBCValues;
r2(FreeIndices) = x;

% Compute gradient and Hessian
[f_r,I,J,S] = build_grad_hess_r(atoms,bonds,r2,z1,k1,TOL_z,maxNumThreads);
f_r = f_r(FreeIndices);
K_r = sparse(I(:),J(:),S(:),length(r2),length(r2));
K_rDBC = K_r(FreeIndices,DBCIndices);
K_r = K_r(FreeIndices,FreeIndices);

end
