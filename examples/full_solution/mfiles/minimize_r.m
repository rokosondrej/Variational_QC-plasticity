function [r2,Niter] = minimize_r(atoms,bonds,r1,z1,k1,DBCIndices,tDBCValues,...
    dtDBCValues,FreeIndices,R0,C,TOL_r,TOL_z,maxNumThreads,solver)

% Solve for x, a vector of free degrees of freedom, using standard Newton algorithm
x = r1(FreeIndices);
eps_r = 1+TOL_r;
Niter = 0;
while eps_r > TOL_r
    Niter = Niter+1;
    switch solver
        case 'am'
            [r2,f_r,K_r] = grad_hess_am(atoms,bonds,x,z1,DBCIndices,tDBCValues,...
                FreeIndices,R0,maxNumThreads);
        case 'con'
            if Niter == 1
                [r2,f_r,K_r,K_rDBC] = grad_hess(atoms,bonds,x,z1,k1,DBCIndices,tDBCValues,...
                    FreeIndices,R0,TOL_z,maxNumThreads);
                f_r = f_r+K_rDBC*dtDBCValues;
            else
                [r2,f_r,K_r] = grad_hess(atoms,bonds,x,z1,k1,DBCIndices,tDBCValues+dtDBCValues,...
                    FreeIndices,R0,TOL_z,maxNumThreads);
            end
    end
    
    % Introduce constraints, use primal-dual formulation, assembly extended quantities
    EK_r = [K_r,C(:,FreeIndices)'
        C(:,FreeIndices),sparse(size(C,1),size(C,1))];
    Ef_r = [f_r;C*r2];
    
    % Solve the system
    du = -EK_r\Ef_r;
    dx = du(1:end-size(C,1));
    lambda = du(end-size(C,1)+1:end);
    x = x+dx;
    
    % Update the error
    eps_r = norm(dx)/norm(x-R0(FreeIndices))+...
        norm(f_r+C(:,FreeIndices)'*lambda);
end

% Reconstruct the converged r-vector from the iterative variable x
r2(DBCIndices) = R0(DBCIndices)+tDBCValues+dtDBCValues;
r2(FreeIndices) = x;

end
