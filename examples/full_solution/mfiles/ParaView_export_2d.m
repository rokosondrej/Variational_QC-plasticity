function ParaView_export_2d(fileName,atoms,bonds,R0,U,Z,K,Time,maxNumThreads)

fprintf('Exporting to vtk... ');t_export = tic;
if ~exist('ParaView','dir')
    mkdir('ParaView');
end
for timeStep = 1:length(Time)
    fin = fopen(['ParaView/out_',fileName,'_',num2str(timeStep,'%.3d'),'.vtu'],'w');
    
    % Beginning of the file
    fprintf(fin,'<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
    fprintf(fin,'<UnstructuredGrid>\n');
    
    % Write points
    fprintf(fin,'<Piece NumberOfPoints="%d" NumberOfCells="%d">\n',length(atoms),length(bonds));
    fprintf(fin,'<Points>\n');
    fprintf(fin,' <DataArray type="Float64" NumberOfComponents="3" format="ascii"> ');
    fprintf(fin,'%e %e %e ',[R0(1:2:end)';R0(2:2:end)';zeros(1,length(atoms))]);
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</Points>\n');
    
    % Write cells
    fprintf(fin,'<Cells>\n');
    fprintf(fin,' <DataArray type="Int32" Name="connectivity" format="ascii"> ');
    
    % 1D truss
    fprintf(fin,'%d %d ',(reshape([bonds(:).Atoms],[2,length(bonds)])-1) ); % 0-offset indices
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
    fprintf(fin,'%d ',2:2:2*length(bonds));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
    fprintf(fin,'%d ',3*ones(1,length(bonds)));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</Cells>\n');
    
    % Write point data
    fprintf(fin,'<PointData Scalars="" Vectors="" Tensors="">\n');
    fprintf(fin,' <DataArray type="Float64" Name="DisplacementVector" NumberOfComponents="3" format="ascii"> ');
    fprintf(fin,'%e %e %e ',[U(1:2:end,timeStep)';U(2:2:end,timeStep)';0*U(1:2:end,timeStep)']);
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</PointData>\n');
    
    % Write cell data
    [eps,stress] = sample_esd_2d(atoms,bonds,R0+U(:,timeStep),full(Z(:,timeStep)),maxNumThreads);
    % Strain
    fprintf(fin,'<CellData Scalars="" Vectors="" Tensors="">\n');
    fprintf(fin,' <DataArray type="Float64" Name="strain" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',eps);
    fprintf(fin,'</DataArray>\n');
    % Stress
    fprintf(fin,' <DataArray type="Float64" Name="stress" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',stress);
    fprintf(fin,'</DataArray>\n');
    % Plastic slip
    fprintf(fin,' <DataArray type="Float64" Name="pl. slip" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',full(Z(:,timeStep)));
    fprintf(fin,'</DataArray>\n');
    % Plastic slip
    fprintf(fin,' <DataArray type="Float64" Name="cum. pl. slip" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',full(K(:,timeStep)));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</CellData>\n');
    
    % Close the VTK file
    fprintf(fin,'</Piece>\n');
    fprintf(fin,'</UnstructuredGrid>\n');
    fprintf(fin,'</VTKFile>');
    
    % Close the file
    fclose(fin);
end
fprintf('in %g s\n',toc(t_export));

end