// build_diss: computes the dissipation part of the incremental energy of
// the system

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
				 const mxArray *prhs[])
{

	// Test for five input argument
	if (nrhs != 5)
	{
		mexErrMsgTxt("Five input arguments required.");
	}

	// Get the input data
	double *prz = mxGetPr(prhs[2]);
	double *prk = mxGetPr(prhs[3]);
	int maxNumThreads = (int)mxGetScalar(prhs[4]);

	// Compute the dissipation distance
	nlhs = 1;
	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
	double *prDiss = mxGetPr(plhs[0]);
	int i;
	double Diss = 0.0;
	int NBonds = (int)mxGetNumberOfElements(prhs[1]);
	int name_Potential = mxGetFieldNumber(prhs[1], "Potential");

#pragma omp parallel num_threads(maxNumThreads)
	{
		int _bond;
		double *_Potential, _z_p, _Zp;

#pragma omp for reduction(+ \
						  : Diss)
		for (i = 0; i < NBonds; i++)
		{				   // loop over all atoms
			_bond = i + 1; // MATLAB indexing

			// Get data for the bond
			_Potential = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Potential));
			_z_p = prz[_bond - 1]; // current plastic elongation
			_Zp = prk[_bond - 1];  // previous-time-step plastic elongation

			// Compute dissipation distance D for a single bond
			Diss += _Potential[2] * fabs(_z_p - _Zp);
		}
	}

	prDiss[0] = Diss;
}
