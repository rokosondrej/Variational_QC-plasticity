function ParaView_export_2d_QC(fileName,atoms,bonds,triangles,samplingatoms,...
    R0,U,Z,K,Time,maxNumThreads)

fprintf('Exporting to vtk... ');t_export = tic;
if ~exist('ParaView','dir')
    mkdir('ParaView');
end
for timeStep = 1:length(Time)
    
    % Get triangulation (renumbered)
    tatoms = [triangles(:).VertexAtoms];
    tatoms = reshape(tatoms,3,length(triangles));
    [tatomids,~,tind] = unique(tatoms(:));
    p = [R0(2*tatomids-1)';R0(2*tatomids)'];
    t = reshape(tind,3,size(tatoms,2));
    Utx = U(2*tatomids-1,timeStep);
    Uty = U(2*tatomids,timeStep);
    Utz = 0*Utx;
    
    % Get sampling bonds (renumbered)
    samplingatomsID = [samplingatoms(:).ID]';
    samplingbondsID = unique([atoms(samplingatomsID).BondList])';
    satoms = [bonds(samplingbondsID).Atoms];
    [satomids,~,sind] = unique(satoms(:));
    sR0x = R0(2*satomids-1);
    sR0y = R0(2*satomids);
    sR0z = 0*sR0x;
    Usx = U(2*satomids-1,timeStep);
    Usy = U(2*satomids,timeStep);
    Usz = 0*Usx;
    sbonds = reshape(sind,2,length(samplingbondsID));
    
    
    
    % Write vtk file
    fin = fopen(['ParaView/out_',fileName,'_',num2str(timeStep,'%.3d'),'.vtu'],'w');
    
    % Beginning of the file
    fprintf(fin,'<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
    fprintf(fin,'<UnstructuredGrid>\n');
    
    
    
    % Write the first piece: sampling interactions
    fprintf(fin,'<Piece NumberOfPoints="%d" NumberOfCells="%d">\n',length(satomids),length(samplingbondsID));
    
    % Write points
    fprintf(fin,'<Points>\n');
    fprintf(fin,' <DataArray type="Float64" NumberOfComponents="3" format="ascii"> ');
    fprintf(fin,'%e %e %e ',[sR0x';sR0y';sR0z']);
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</Points>\n');
    
    % Write cells
    fprintf(fin,'<Cells>\n');
    fprintf(fin,' <DataArray type="Int32" Name="connectivity" format="ascii"> ');
    
    % 1D truss
    fprintf(fin,'%d %d ',(sbonds-1)); % 0-offset indices
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
    fprintf(fin,'%d ',2:2:2*length(bonds));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
    fprintf(fin,'%d ',3*ones(1,length(bonds)));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</Cells>\n');
    
    % Write point data
    fprintf(fin,'<PointData Scalars="" Vectors="" Tensors="">\n');
    fprintf(fin,' <DataArray type="Float64" Name="DisplacementVector" NumberOfComponents="3" format="ascii"> ');
    fprintf(fin,'%e %e %e ',[Usx';Usy';Usz']);
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</PointData>\n');
    
    % Write cell data
    [eps,stress] = sample_esd_2d_QC(atoms,bonds,R0+U(:,timeStep),full(Z(:,timeStep)),maxNumThreads);
    % Strain
    fprintf(fin,'<CellData Scalars="" Vectors="" Tensors="">\n');
    fprintf(fin,' <DataArray type="Float64" Name="strain" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',eps(samplingbondsID));
    fprintf(fin,'</DataArray>\n');
    % Stress
    fprintf(fin,' <DataArray type="Float64" Name="stress" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',stress(samplingbondsID));
    fprintf(fin,'</DataArray>\n');
    % Plastic slip
    fprintf(fin,' <DataArray type="Float64" Name="pl. slip" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',full(Z(samplingbondsID,timeStep)));
    fprintf(fin,'</DataArray>\n');
    % Cumulative plastic slip
    fprintf(fin,' <DataArray type="Float64" Name="cum. pl. slip" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',full(K(samplingbondsID,timeStep)));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</CellData>\n');
    fprintf(fin,'</Piece>\n');
    
    
    % Write the second piece: triangles
    fprintf(fin,'<Piece NumberOfPoints="%d" NumberOfCells="%d">\n',size(p,2),size(t,2));
    
    % Write points: coordinates
    fprintf(fin,'<Points>\n');
    fprintf(fin,' <DataArray type="Float64" NumberOfComponents="3" format="ascii"> ');
    fprintf(fin,'%e %e %e ',[p;zeros(1,size(p,2))]);
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</Points>\n');
    
    % Write cells: linear triangles
    fprintf(fin,'<Cells>\n');
    fprintf(fin,' <DataArray type="Int32" Name="connectivity" format="ascii"> ');
    fprintf(fin,'%d %d %d ',(t(:)-1)); % 0-offset indices
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
    fprintf(fin,'%d ',3:3:3*size(t,2));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
    fprintf(fin,'%d ',5*ones(1,size(t,2)));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</Cells>\n');
    
    % Write point data: displacements
    fprintf(fin,'<PointData Scalars="" Vectors="" Tensors="">\n');
    fprintf(fin,' <DataArray type="Float64" Name="DisplacementVector" NumberOfComponents="3" format="ascii"> ');
    fprintf(fin,'%e %e %e ',[Utx';Uty';Utz']);
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</PointData>\n');
    
    % Write cell data
    % Strain
    fprintf(fin,'<CellData Scalars="" Vectors="" Tensors="">\n');
    fprintf(fin,' <DataArray type="Float64" Name="strain" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',zeros(size(t,2),1));
    fprintf(fin,'</DataArray>\n');
    % Stress
    fprintf(fin,' <DataArray type="Float64" Name="stress" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',zeros(size(t,2),1));
    fprintf(fin,'</DataArray>\n');
    % Plastic stlip
    fprintf(fin,' <DataArray type="Float64" Name="pl. slip" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',zeros(size(t,2),1));
    fprintf(fin,'</DataArray>\n');
    % Cumulative plastic stlip
    fprintf(fin,' <DataArray type="Float64" Name="cum. pl. slip" NumberOfComponents="1" format="ascii"> ');
    fprintf(fin,'%e ',zeros(size(t,2),1));
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</CellData>\n');
    fprintf(fin,'</Piece>\n');
    
    
    
    % Close the VTK file
    fprintf(fin,'</UnstructuredGrid>\n');
    fprintf(fin,'</VTKFile>');
    
    % Close the file
    fclose(fin);
end
fprintf('in %g s\n',toc(t_export));

end