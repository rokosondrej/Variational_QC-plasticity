function [r2,G,H,H_DBC] = grad_hess_am_QC(atoms,samplingatoms,bonds,x,z1,...
    DBCIndicesQC,tDBCValuesQC,FreeIndicesQC,R0QC,Phi,maxNumThreads)

% Reconstruct r
r2 = zeros(size(R0QC));
r2(DBCIndicesQC) = R0QC(DBCIndicesQC)+tDBCValuesQC;
r2(FreeIndicesQC) = x;
r = Phi*r2;

% Compute gradient and Hessian
[f_r,I,J,S] = build_grad_hess_r_am_QC(atoms,samplingatoms,bonds,r,z1,maxNumThreads);
G = Phi'*f_r;
G = G(FreeIndicesQC);
K_r = sparse(I(:),J(:),S(:),length(r),length(r));
H = Phi'*K_r*Phi;
H_DBC = H(FreeIndicesQC,DBCIndicesQC);
H = H(FreeIndicesQC,FreeIndicesQC);

end
