%% Compile all the *.mex files
% 0 - compile for release, 1 - compile for debugging
debugMode = 0;
clc;

%% Compiling the source codes
disp('Compiling mex files:');
disp('----------------------');
cd mex;
delete('*.mexa64');
delete('*.mexw64');
string = dir('*.cpp');
if debugMode == 0
    if ispc
        for i = 1:length(string)
            disp(['compiling ',string(i).name]);
            eval(['mex ',string(i).name,' -largeArrayDims COMPFLAGS="/openmp $COMPFLAGS"'])
            fprintf('\n');
        end
    elseif isunix
        for i = 1:length(string)
            disp(['compiling ',string(i).name]);
            eval(['mex ',string(i).name,' -largeArrayDims CXXOPTIMFLAGS="-O3 -fwrapv -DNDEBUG" CXXFLAGS="\$CXXFLAGS -fopenmp -Wall" LDFLAGS="\$LDFLAGS -fopenmp"'])
            fprintf('\n');
        end
    end
elseif debugMode == 1
    for i = 1:length(string)
        disp(['compiling ',string(i).name]);
        eval(['mex -v -g ',string(i).name,' -largeArrayDims CXXFLAGS="\$CXXFLAGS -Wall"'])
        fprintf('\n');
    end
end
clear debugMode;
cd ..;
