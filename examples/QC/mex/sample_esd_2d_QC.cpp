// sample_esd_2d: compute total strain, stress, and

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

    // Test for five input arguments
    if (nrhs != 5)
        mexErrMsgTxt("Five input arguments required!\n");

    // Get the input data
    double *prr = mxGetPr(prhs[2]);
    double *prz = mxGetPr(prhs[3]);
    int maxNumThreads = (int)mxGetScalar(prhs[4]);

    // Minimize bond-wise energies
    nlhs = 2;
    int i;
    int NBonds = (int)mxGetNumberOfElements(prhs[1]);
    plhs[0] = mxCreateDoubleMatrix(NBonds, 1, mxREAL);
    plhs[1] = mxCreateDoubleMatrix(NBonds, 1, mxREAL);
    double *prstrain = mxGetPr(plhs[0]); // pointer to total strain
    double *prstress = mxGetPr(plhs[1]); // pointer to total stress
    int name_R = mxGetFieldNumber(prhs[0], "R");
    int name_Atoms = mxGetFieldNumber(prhs[1], "Atoms");
    int name_Potential = mxGetFieldNumber(prhs[1], "Potential");

#pragma omp parallel num_threads(maxNumThreads)
    {
        int _alpha, _beta, _bond;
        double _R, _r, _eps, _z_p, _dphi;
        double *_Ra, *_Rb, *_Potential, *_Atoms, _Rab[2], _rab[2];

#pragma omp for
        for (i = 0; i < NBonds; i++)
        {                          // loop over all bonds
            _bond = i + 1;         // bond ID, MATLAB indexing
            _z_p = prz[_bond - 1]; // current plastic slip

            // Get data for atoms alpha and beta, connected by the bond
            _Atoms = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Atoms));
            _alpha = (int)_Atoms[0]; // atom alpha, MATLAB indexing
            _beta = (int)_Atoms[1];  // atom beta, MATLAB indexing
            _Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
            _Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
            _Rab[0] = _Rb[0] - _Ra[0];
            _Rab[1] = _Rb[1] - _Ra[1];
            _rab[0] = prr[2 * (_beta - 1)] - prr[2 * (_alpha - 1)];
            _rab[1] = prr[2 * (_beta - 1) + 1] - prr[2 * (_alpha - 1) + 1];
            _R = sqrt(_Rab[0] * _Rab[0] + _Rab[1] * _Rab[1]); // the initial bond length
            _r = sqrt(_rab[0] * _rab[0] + _rab[1] * _rab[1]); // deformed bond length

            // Get data for the bond between atoms alpha and beta
            _Potential = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Potential));

            // Compute strain
            _eps = (_r - _R) / _R; // bond strain

            // Compute dphi = \phi' (corresponds to stress, as we assume unit area)
            _dphi = (_Potential[0] / _R) * (_r - _R - _z_p);

            // Write strain
#pragma omp atomic
            prstrain[i] += _eps;

            // Write stress
#pragma omp atomic
            prstress[i] += _dphi;
        }
    }
}
