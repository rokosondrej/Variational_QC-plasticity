// build_hess_r_QC: assembly the Hessian of approximate internal
// energy w.r.t. r using summation rule

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/******************* Definition of constants ******************/
const int MAXITER = 100; // maximum No. of Newton iterations

/******************** Function declarations *******************/
int sgn(double x); // signum function sgn(x)

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for eight input arguments
	if (nrhs != 8)
	{
		mexErrMsgTxt("Eight input arguments required.");
	}

	// Get the input data
	double *prr = mxGetPr(prhs[3]);
	double *prz = mxGetPr(prhs[4]);
	double *prk = mxGetPr(prhs[5]);
	double TOL_z = mxGetScalar(prhs[6]);
	int maxNumThreads = (int)mxGetScalar(prhs[7]);
	int NAtoms = (int)mxGetNumberOfElements(prhs[0]);
	int NsAtoms = (int)mxGetNumberOfElements(prhs[1]);

	// Allocate outputs
	nlhs = 4;
	plhs[0] = mxCreateDoubleMatrix(2 * NAtoms, 1, mxREAL);
	plhs[1] = mxCreateDoubleMatrix(16, 8 * NsAtoms, mxREAL);
	plhs[2] = mxCreateDoubleMatrix(16, 8 * NsAtoms, mxREAL);
	plhs[3] = mxCreateDoubleMatrix(16, 8 * NsAtoms, mxREAL);
	double *prGrad = mxGetPr(plhs[0]);
	double *prI = mxGetPr(plhs[1]);
	double *prJ = mxGetPr(plhs[2]);
	double *prS = mxGetPr(plhs[3]);

	// Compute the Hessian and populate prI, prJ, prS
	int i, status = 0;
	int counter = 0;
	int name_R = mxGetFieldNumber(prhs[0], "R");
	int name_NeighbourList = mxGetFieldNumber(prhs[0], "NeighbourList");
	int name_BondList = mxGetFieldNumber(prhs[0], "BondList");
	int name_Potential = mxGetFieldNumber(prhs[2], "Potential");
	int name_ID = mxGetFieldNumber(prhs[1], "ID");
	int name_w = mxGetFieldNumber(prhs[1], "w");

#pragma omp parallel num_threads(maxNumThreads)
	{
		int _j, _m, _n, _alpha, _beta, _bond;
		double *_Ra, *_Rb, *_NeighbourList, *_BondList, *_prID, *_Potential, *_prw;
		double _ra[2], _rb[2], _rab[2], _fab[2], _R, _Zp, _Zc, _r, _r2, _w;
		double _dphi, _ddphi, _dphir, _ddphir2, _dphir3, _ftri, _Yieldtri, _z_p, _z_c, _dZp;
		double _Kbond[4][4];						 // local stiffness matrix for a single bond
		int _Ln[4];									 // vector of code numbers
		int *_prI = new int[16 * 8 * NsAtoms];		 // thread-private row indices
		int *_prJ = new int[16 * 8 * NsAtoms];		 // thread-private column indices
		double *_prS = new double[16 * 8 * NsAtoms]; // thread-private data
		int _counter = 0;							 // thread-private bond counter
		mxArray *_pNeighbourList;

#pragma omp for
		for (i = 0; i < NsAtoms; i++)
		{ // loop over all sampling atoms
			_prw = mxGetPr(mxGetFieldByNumber(prhs[1], i, name_w));
			_prID = mxGetPr(mxGetFieldByNumber(prhs[1], i, name_ID));
			_w = _prw[0];
			_alpha = (int)_prID[0]; // atom alpha ID, MATLAB indexing

			// Get data for a sampling atom alpha
			_Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
			_pNeighbourList = mxGetFieldByNumber(prhs[0], _alpha - 1, name_NeighbourList);
			_NeighbourList = mxGetPr(_pNeighbourList);
			_BondList = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_BondList));
			_ra[0] = prr[2 * (_alpha - 1)];
			_ra[1] = prr[2 * (_alpha - 1) + 1];
			for (_j = 0; _j < (int)mxGetN(_pNeighbourList); _j++)
			{									 // loop over all the nearest neighbours
				_beta = (int)_NeighbourList[_j]; // atom beta ID, MATLAB indexing

				// Get data for atom beta
				_Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
				_rb[0] = prr[2 * (_beta - 1)];
				_rb[1] = prr[2 * (_beta - 1) + 1];
				_R = sqrt(pow(_Rb[0] - _Ra[0], 2) +
						  pow(_Rb[1] - _Ra[1], 2)); // the initial bond length
				_rab[0] = _rb[0] - _ra[0];			// vector connecting atoms alpha and beta
				_rab[1] = _rb[1] - _ra[1];
				_r2 = pow(_rab[0], 2) + pow(_rab[1], 2);
				_r = sqrt(_r2); // bond length

				// Get data for the bond connecting atoms alpha and beta
				_bond = (int)_BondList[_j]; // MATLAB indexing
				_Potential = mxGetPr(mxGetFieldByNumber(prhs[2], _bond - 1, name_Potential));
				_Zp = prz[_bond - 1]; // previous time-step plastic elongation
				_Zc = prk[_bond - 1]; // previous-time-step cumulative plastic elongation

				/*<<<<<<<<<<<<<<<<<<<<<<<< Computation of internal variables >>>>>>>>>>>>>>>>>>>>>>>>*/
				// Nonlinear hardeing
				_ftri = (_Potential[0] / _R) * (_r - _R - _Zp); // compute elastic trial force
				_Yieldtri = fabs(_ftri) - _Potential[2] * (1 + _Potential[1] * pow(_Zc / _R, _Potential[3]));

				// Elastic step
				if (_Yieldtri <= 0.0)
				{
					_z_p = _Zp;
					_z_c = _Zc;
				}

				// Plastic step - return-mapping
				else
				{
					int _Niter = 0;
					double _x = 1e-14; // perturb searched value
					double _dx, _dgx;
					double _gx = fabs(_ftri) - _x * (_Potential[0] / _R) - _Potential[2] * (1 + _Potential[1] * pow((_Zc + _x) / _R, _Potential[3]));
					while ((fabs(_gx) > TOL_z) || (_Niter < 1))
					{ // 1-D Newton iteration
						_Niter++;
						_dgx = -(_Potential[0] / _R) - _Potential[3] * _Potential[2] * _Potential[1] * pow((_Zc + _x) / _R, _Potential[3] - 1) / _R;
						_dx = -_gx / _dgx;
						_x += _dx;
						_gx = fabs(_ftri) - _x * (_Potential[0] / _R) - _Potential[2] * (1 + _Potential[1] * pow((_Zc + _x) / _R, _Potential[3]));

						// Check convergence of the return mapping algorithm
						if (_Niter > MAXITER)
						{
#pragma omp atomic
							++status;
							break;
						}
					}
					_dZp = _x > 0.0 ? _x : 0.0; // allow only for positive increments
					_z_p = _Zp + _dZp * sgn(_ftri);
					_z_c = _Zc + _dZp;
				}

				/*<<<<<<<<<<<<<<<<<<<<<<<< Build grad >>>>>>>>>>>>>>>>>>>>>>>>*/
				// Compute dphi = \phi', and ddphi = \phi''
				_dphi = 0.5 * (_Potential[0] / _R) * (_r - _R - _z_p);
				_ddphi = 0.5 * _Potential[0] / _R;

				// Compute force components
				_fab[0] = -_w * _dphi * _rab[0] / _r;
				_fab[1] = -_w * _dphi * _rab[1] / _r;

				// Allocate fa to Grad
#pragma omp atomic
				prGrad[2 * (_alpha - 1)] += _fab[0];
#pragma omp atomic
				prGrad[2 * (_alpha - 1) + 1] += _fab[1];

				// Allocate to Grad the force acting on atom beta
#pragma omp atomic
				prGrad[2 * (_beta - 1)] -= _fab[0];
#pragma omp atomic
				prGrad[2 * (_beta - 1) + 1] -= _fab[1];

				/*<<<<<<<<<<<<<<<<<<<<<<<< Build Hess >>>>>>>>>>>>>>>>>>>>>>>>*/
				// Construct _Kbond - hessian matrix of a single bond
				_dphir = _dphi / _r;
				_ddphir2 = _ddphi / _r2;
				_dphir3 = _dphi / (_r2 * _r);
				// gamma = alpha, delta = alpha
				_Kbond[0][0] = (_dphir * 1 + (_ddphir2 - _dphir3) * (_rab[0] * _rab[0]));
				_Kbond[0][1] = (_dphir * 0 + (_ddphir2 - _dphir3) * (_rab[0] * _rab[1]));
				_Kbond[1][0] = (_dphir * 0 + (_ddphir2 - _dphir3) * (_rab[1] * _rab[0]));
				_Kbond[1][1] = (_dphir * 1 + (_ddphir2 - _dphir3) * (_rab[1] * _rab[1]));
				// Correction due to changing internal variable
				if (_z_c > _Zc)
				{
					// Change of z w.r.t. r
					double _dgdr = (_Potential[0] / _R);
					double _dgdp = -(_Potential[0] / _R) - _Potential[3] * _Potential[2] * _Potential[1] * pow(_z_c / _R, _Potential[3] - 1) / _R;
					double _dpdz = -0.5 * (_Potential[0] * _dgdr) / (_dgdp * _r2 * _R);
					_Kbond[0][0] -= _dpdz * (_rab[0] * _rab[0]);
					_Kbond[0][1] -= _dpdz * (_rab[0] * _rab[1]);
					_Kbond[1][0] -= _dpdz * (_rab[1] * _rab[0]);
					_Kbond[1][1] -= _dpdz * (_rab[1] * _rab[1]);
				}
				// gamma = beta, delta = beta
				_Kbond[2][2] = _Kbond[0][0];
				_Kbond[2][3] = _Kbond[0][1];
				_Kbond[3][2] = _Kbond[1][0];
				_Kbond[3][3] = _Kbond[1][1];
				// gamma = alpha, delta = beta
				_Kbond[0][2] = -_Kbond[0][0];
				_Kbond[0][3] = -_Kbond[0][1];
				_Kbond[1][2] = -_Kbond[1][0];
				_Kbond[1][3] = -_Kbond[1][1];
				// gamma = beta, delta = alpha
				_Kbond[2][0] = _Kbond[0][2];
				_Kbond[2][1] = _Kbond[0][3];
				_Kbond[3][0] = _Kbond[1][2];
				_Kbond[3][1] = _Kbond[1][3];

				// Allocate _Kbond into _prI, _prJ, and _prS
				_Ln[0] = 2 * _alpha - 1; // indexing for MATLAB
				_Ln[1] = 2 * _alpha;
				_Ln[2] = 2 * _beta - 1;
				_Ln[3] = 2 * _beta;
				for (_n = 0; _n < 4; _n++)
				{
					for (_m = 0; _m < 4; _m++)
					{
						_prI[_counter * 16 + _n * 4 + _m] = _Ln[_m];
						_prJ[_counter * 16 + _n * 4 + _m] = _Ln[_n];
						_prS[_counter * 16 + _n * 4 + _m] = _w * _Kbond[_m][_n]; // multiply by the weight factor
					}
				}
				_counter++;
			}
		}

		// Collect _prI, _prJ, _prS from all threads
#pragma omp critical(ALLOCATE)
		{
			for (_j = 0; _j < _counter; _j++)
			{
				for (_m = 0; _m < 16; _m++)
				{
					prI[(counter + _j) * 16 + _m] = _prI[_j * 16 + _m];
					prJ[(counter + _j) * 16 + _m] = _prJ[_j * 16 + _m];
					prS[(counter + _j) * 16 + _m] = _prS[_j * 16 + _m];
				}
			}
			counter += _counter;
			delete[] _prI;
			delete[] _prJ;
			delete[] _prS;
		}
	}

	// Check convergence of the return mapping algorithm
	if (status > 0)
		mexWarnMsgTxt("Return mapping did not converge!");

	// Resize the outputs
	mxSetN(plhs[1], counter);
	mxSetN(plhs[2], counter);
	mxSetN(plhs[3], counter);
}

/******************** Function definitions ********************/
// Signum function
int sgn(double x)
{
	return (x > 0) - (x < 0);
}